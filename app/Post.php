<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //


    //protected $guarded = ['id'];
    protected $fillable = ['title', 'excerpt', 'body'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
