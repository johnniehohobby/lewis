<?php

use App\Post;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\ModelNotFoundException;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $posts = Post::all();


    return view('posts', [
        'posts' => $posts
    ]);
});


Route::get('posts/{post}', function(Post $post) {

    //find a post by its slug and pass it to a view called "post"
    return view('post', [
        'post' => $post
    ]);

    
});